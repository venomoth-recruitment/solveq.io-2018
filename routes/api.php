<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/accounts', 'AccountController@index');
Route::get('/accounts/{accountNumber}', 'AccountController@show');
Route::post('/accounts', 'AccountController@store');
Route::put('/accounts/{accountNumber}', 'AccountController@update');
Route::delete('/accounts/{accountNumber}', 'AccountController@destroy');

Route::get('/balances/{accountNumber}', 'BalanceController@show');

Route::post('/deposits', 'DepositController@store');

Route::post('/withdrawals', 'WithdrawalController@store');

Route::post('/transfers', 'TransferController@store');

Route::get('/transactions/{accountNumber}', 'TransactionController@show');
