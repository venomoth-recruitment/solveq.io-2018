# Recruitment Solveq

Recruitment Solveq is recruitment task for Solveq company.

It implements REST API that allows manage bank accounts and perform operations on them.

# Table of Contents

- [Possible operations](#possible-operations)
- [License](#license)

## Possible operations

#### `GET /api/accounts`

Returns all accounts and theirs details.

#### `GET /api/accounts/{accountNumber}`

Returns account details.

#### `POST /api/accounts`

Adds account. Required parameters:

* number
* first_name
* last_name
* email

#### `PUT /api/accounts/{accountNumber}`

Edits account specified by accountNumber. Required parameters:

* first_name
* last_name
* email

#### `DELETE /api/accounts/{accountNumber}`

Removes account specified by accountNumber.

#### `GET /api/balances/{accountNumber}`

Returns account balance specified by accountNumber.

#### `POST /api/deposits`

Adds deposit. Required parameters:

* account_number
* amount

#### `POST /api/withdrawals`

Adds withdrawal. Required parameters:
                 
* account_number
* amount

#### `POST /api/transfers`

Adds transfer. Required parameters:
               
* source_account_number
* destination_account_number
* amount
* title

#### `GET /api/transactions/{accountNumber}`

Returns account transactions specified by accountNumber.

## License

Recruitment Solveq is released under the MIT Licence. See the bundled LICENSE file for details.
