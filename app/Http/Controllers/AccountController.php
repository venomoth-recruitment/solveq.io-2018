<?php

namespace App\Http\Controllers;

use App\Account;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function index()
    {
        return Account::get(['number', 'first_name', 'last_name', 'email']);
    }

    public function show($accountNumber, Request $request)
    {
        $request->request->add(['account_number' => $accountNumber]);

        $validatedRequest = $request->validate([
            'account_number' => [
                'required',
                'regex:/^[0-9]{26}$/',
                'exists:accounts,number,deleted_at,NULL'
            ]
        ]);

        return Account::find($validatedRequest['account_number'], ['number', 'first_name', 'last_name', 'email']);
    }

    public function store(Request $request)
    {
        $validatedRequest = $request->validate([
            'number'     => [
                'required',
                'regex:/^[0-9]{26}$/'
            ],
            'first_name' => [
                'required',
                'string',
                'min:2',
                'max:255'
            ],
            'last_name'  => [
                'required',
                'string',
                'min:2',
                'max:255'
            ],
            'email'      => [
                'required',
                'email',
                'max:255'
            ]
        ]);

        $account = new Account();
        $account->number = $validatedRequest['number'];
        $account->first_name = $validatedRequest['first_name'];
        $account->last_name = $validatedRequest['last_name'];
        $account->email = $validatedRequest['email'];
        $account->save();
    }

    public function update($accountNumber, Request $request)
    {
        $request->request->add(['account_number' => $accountNumber]);

        $validatedRequest = $request->validate([
            'account_number' => [
                'required',
                'regex:/^[0-9]{26}$/',
                'exists:accounts,number,deleted_at,NULL'
            ],
            'first_name'     => [
                'required',
                'string',
                'min:2',
                'max:255'
            ],
            'last_name'      => [
                'required',
                'string',
                'min:2',
                'max:255'
            ],
            'email'          => [
                'required',
                'email',
                'max:255'
            ]
        ]);

        $account = Account::find($validatedRequest['account_number']);
        $account->first_name = $validatedRequest['first_name'];
        $account->last_name = $validatedRequest['last_name'];
        $account->email = $validatedRequest['email'];
        $account->save();
    }

    public function destroy($accountNumber, Request $request)
    {
        $request->request->add(['account_number' => $accountNumber]);

        $validatedRequest = $request->validate([
            'account_number' => [
                'required',
                'regex:/^[0-9]{26}$/',
                'exists:accounts,number,deleted_at,NULL'
            ]
        ]);

        $account = Account::find($validatedRequest['account_number']);
        $account->delete();
    }
}
