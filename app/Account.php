<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
    use SoftDeletes;

    public $incrementing = false;

    protected $primaryKey = 'number';

    protected $appends = ['balance'];

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'source_account_number', 'number')->latest()->orderByDesc('id');
    }

    public function getLastTransactionAttribute()
    {
        return $this->transactions()->first();
    }

    public function getBalanceAttribute()
    {
        $balance = 0;

        if ($this->lastTransaction !== null) {
            $balance = $this->lastTransaction->balance_after;
        }

        return $balance;
    }
}
