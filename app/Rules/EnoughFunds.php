<?php

namespace App\Rules;

use App\Account;
use Illuminate\Contracts\Validation\Rule;

class EnoughFunds implements Rule
{
    /**
     * @var string
     */
    protected $accountNumber;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(string $accountNumber)
    {
        $this->accountNumber = $accountNumber;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $account = Account::find($this->accountNumber);

        if ($account === null) {
            return false;
        }

        if ((float)$value > (float)$account->balance) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'There is not enough funds.';
    }
}
