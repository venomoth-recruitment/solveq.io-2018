<?php

namespace Tests\Feature\Api\Withdrawals;

use App\Account;
use App\Transaction;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WithdrawalPostTest extends TestCase
{
    use RefreshDatabase;

    public function testRequiredParameters()
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/withdrawals');

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['account_number', 'amount']);
    }

    /**
     * @dataProvider invalidAccountNumberDataProvider
     */
    public function testInvalidAccountNumber($accountNumber)
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/withdrawals', [
                'account_number' => $accountNumber
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['account_number']);
    }

    public function invalidAccountNumberDataProvider()
    {
        return [
            ['1234567890123456789012345'],
            ['123456789012345678901234567']
        ];
    }

    public function testNonExistingAccountNumber()
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/withdrawals', [
                'account_number' => '12345678901234567890123456'
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['account_number']);
    }

    public function testDeletedAccountNumber()
    {
        $account = factory(Account::class)->create();
        $account->delete();

        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/withdrawals', [
                'account_number' => $account->number
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['account_number']);
    }

    /**
     * @dataProvider invalidAmountDataProvider
     */
    public function testInvalidAmount($amount)
    {
        $account = factory(Account::class)->create();

        factory(Transaction::class)->create([
            'source_account_number'      => $account->number,
            'destination_account_number' => null,
            'amount'                     => 999999999999.99,
            'balance_after'              => 999999999999.99,
            'title'                      => ''
        ]);

        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/withdrawals', [
                'account_number' => $account->number,
                'amount'         => $amount
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['amount']);
    }

    public function invalidAmountDataProvider()
    {
        return [
            [''],
            ['foo'],
            ['1foo'],
            ['foo1'],
            [0],
            [0.001],
            [0.009],
            [1000000000000]
        ];
    }

    public function testNotEnoughFundsWithoutTransactions()
    {
        $account = factory(Account::class)->create();

        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/withdrawals', [
                'account_number' => $account->number,
                'amount'         => 10
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['amount']);
    }

    public function testNotEnoughFundsWithTransactions()
    {
        $amount = 9.99;

        $account = factory(Account::class)->create();

        factory(Transaction::class)->create([
            'source_account_number'      => $account->number,
            'destination_account_number' => null,
            'amount'                     => $amount,
            'balance_after'              => $amount,
            'title'                      => ''
        ]);

        factory(Transaction::class)->create([
            'source_account_number' => $account->number,
            'amount'                => round($amount * -1, 2),
            'balance_after'         => 0,
            'title'                 => 'transfer'
        ]);

        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/withdrawals', [
                'account_number' => $account->number,
                'amount'         => $amount
            ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['amount']);
    }

    public function testValidRequest()
    {
        $amount = 10;
        $amount2 = 9.99;

        $account = factory(Account::class)->create();

        factory(Transaction::class)->create([
            'source_account_number'      => $account->number,
            'destination_account_number' => null,
            'amount'                     => $amount,
            'balance_after'              => $amount,
            'title'                      => ''
        ]);

        factory(Transaction::class)->create([
            'source_account_number'      => $account->number,
            'destination_account_number' => null,
            'amount'                     => $amount2,
            'balance_after'              => round($amount + $amount2, 2),
            'title'                      => ''
        ]);

        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/withdrawals', [
                'account_number' => $account->number,
                'amount'         => $amount
            ]);

        $response->assertStatus(200);
        $this->assertSame('', $response->getContent());

        $this->assertDatabaseHas('transactions', [
            'source_account_number'      => $account->number,
            'destination_account_number' => null,
            'amount'                     => round($amount * -1, 2),
            'title'                      => '',
            'balance_after'              => $amount2,
        ]);

        $response = $this->withHeader('Accept', 'application/json')
            ->post('/api/withdrawals', [
                'account_number' => $account->number,
                'amount'         => $amount2
            ]);

        $response->assertStatus(200);
        $this->assertSame('', $response->getContent());

        $this->assertDatabaseHas('transactions', [
            'source_account_number'      => $account->number,
            'destination_account_number' => null,
            'amount'                     => round($amount2 * -1, 2),
            'title'                      => '',
            'balance_after'              => 0,
        ]);
    }
}
