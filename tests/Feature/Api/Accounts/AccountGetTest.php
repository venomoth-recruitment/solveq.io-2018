<?php

namespace Tests\Feature\Api\Accounts;

use App\Account;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AccountGetTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @dataProvider invalidAccountNumberDataProvider
     */
    public function testInvalidAccountNumber($accountNumber)
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->get('/api/accounts/' . $accountNumber);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['account_number']);
    }

    public function invalidAccountNumberDataProvider()
    {
        return [
            ['1234567890123456789012345'],
            ['123456789012345678901234567']
        ];
    }

    public function testNonExistingAccountNumber()
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->get('/api/accounts/12345678901234567890123456');

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['account_number']);
    }

    public function testDeletedAccountNumber()
    {
        $account = factory(Account::class)->create();
        $account->delete();

        $response = $this->withHeader('Accept', 'application/json')
            ->get('/api/accounts/' . $account->number);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['account_number']);
    }

    public function testValidRequest()
    {
        $account = factory(Account::class)->create();

        $response = $this->withHeader('Accept', 'application/json')
            ->get('/api/accounts/' . $account->number);

        $account->makeHidden(['created_at', 'updated_at']);

        $response->assertStatus(200);
        $response->assertJson($account->toArray());
    }
}
