<?php

namespace Tests\Feature\Api\Accounts;

use App\Account;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AccountsGetTest extends TestCase
{
    use RefreshDatabase;

    public function testEmptyAccounts()
    {
        $response = $this->withHeader('Accept', 'application/json')
            ->get('/api/accounts');

        $response->assertStatus(200);
        $response->assertJson([]);
    }

    public function testNotEmptyAccounts()
    {
        $account = factory(Account::class)->create();
        $account2 = factory(Account::class)->create();
        $account3 = factory(Account::class)->create();
        $account4 = factory(Account::class)->create();
        $account5 = factory(Account::class)->create();

        $account->makeHidden(['created_at', 'updated_at']);
        $account2->makeHidden(['created_at', 'updated_at']);
        $account3->makeHidden(['created_at', 'updated_at']);
        $account4->makeHidden(['created_at', 'updated_at']);
        $account5->makeHidden(['created_at', 'updated_at']);

        $collection = new Collection([$account, $account2, $account3, $account4, $account5]);

        $response = $this->withHeader('Accept', 'application/json')
            ->get('/api/accounts');

        $response->assertStatus(200);
        $response->assertJson($collection->toArray());
    }
}
